<?php
/**
 * @package    logoGrid
 *
 * @author     Jan Dosoudil <czdosi@gmail.com>
 * @copyright  Jan Dosoudil
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       https://bitbucket.org/czdosi/
 */

defined('_JEXEC') or die;

use Joomla\CMS\Application\CMSApplication;
use Joomla\CMS\Plugin\CMSPlugin;

/**
 * LogoGrid plugin.
 *
 * @package   logoGrid
 * @since     1.0.0
 */
class plgContentLogoGrid extends CMSPlugin
{
	/**
	 * Application object
	 *
	 * @var    CMSApplication
	 * @since  1.0.0
	 */
	protected $app;

	/**
	 * Database object
	 *
	 * @var    JDatabaseDriver
	 * @since  1.0.0
	 */
	protected $db;

	/**
	 * Affects constructor behavior. If true, language files will be loaded automatically.
	 *
	 * @var    boolean
	 * @since  1.0.0
	 */
	protected $autoloadLanguage = true;

	public function onContentPrepare($context, &$article, &$articleParams, $page = 0)
	{
		// Don't run this plugin when the content is being indexed
		if ($context == 'com_finder.indexer') {
			return true;
		}

		// Simple performance check to determine whether bot should process further
		if (strpos($article->text, 'logoGrid') === false) {
			return true;
		}

		$regex = '/{logoGrid}(.*?){\/logoGrid}/i';

		// Find all instances of plugin and put in $matches for loadposition
		// $matches[0] is full pattern match, $matches[1] is the position
		preg_match_all($regex, $article->text, $matches, PREG_SET_ORDER);

		$baseFolder = trim("{$this->params->get('baseFolder')}");
		$gidClass = trim($this->params->get('gidClass'));
		$cellClass = trim($this->params->get('cellClass'));

		// No matches, skip this
		if ($matches) {
			if ($this->params->get("loadCss")) {
				$doc = JFactory::getDocument();
				$doc->addStyleSheet(JUri::root() . "plugins/{$this->_type}/{$this->_name}/style.css");
			}

			foreach ($matches as $match) {
				if (empty($match[1])) continue;

				$fileName = "$baseFolder/" . trim($match[1]);

				if (($handle = fopen(JPATH_SITE . "/{$fileName}", "r")) !== FALSE) {
					$fileDir = dirname($fileName);

					$output = "<div class='$gidClass'>\n";
					while (($data = fgetcsv($handle)) !== FALSE) {
						$cnt = count($data);
						if ($cnt < 1) continue;

						$output .= "<div class='$cellClass'>\n";

						$title = ($cnt > 2 && !empty($data[2])) ? trim($data[2]) : null;
						$img_file = trim($data[0]);

						$image = JHtml::image("{$fileDir}/{$img_file}", $title, array('title' => $title));
						if ($cnt > 1 && !empty($data[1])) {
							$output .= JHtml::link(trim($data[1]), $image, array('target' => '_blank'));
						} else {
							$output .= $image;
						}

						$output .= "</div>\n";
					}

					$output .= "</div>\n";

					// We should replace only first occurrence in order to allow positions with the same name to regenerate their content:
					$rep = preg_replace("|$match[0]|", addcslashes($output, '\\$'), $article->text, 1);
					$article->text = $rep;

					fclose($handle);
				}
			}
		}

		return null;
	}
}
